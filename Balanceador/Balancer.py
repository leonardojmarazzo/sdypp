import socket
import threading
import json
import time
import random
import string
import TokenGenerator

servers = {}
clientsperServer = {}
bdFree = {}

def atendServers(host, port):
    #inicio de server
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen(5)
    bdThread = threading.Thread(target=bdCheck)
    bdThread.start()
    while True:
        c, addr = s.accept()
        t = threading.Thread(target=serversThread, args=(c,addr,))
        t.start()

def searchServerID(ip, port):
    for serverID in servers.keys():
        ipport = servers[serverID]
        if ip == ipport[0] and port == ipport[1]:
            return serverID
    return -1

def bdCheck():
    global bdFree
    while True:
        
        for key in bdFree.keys():
            print("Checkin db lock")
            if bdFree[key] != -1:
                serverID = bdFree[key]
                try:
                    ipport = servers[serverID]
                    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    s.connect((ipport[2], ipport[3]))
                    #print('Connected')
                    data = "check in"
                    s.send(data.encode())
                    res = s.recv(1024).decode()
                    if 'alive' not in res:
                        bdFree[key] = -1
                        del servers[serverID]
                    s.close()
                except:
                    #print("error conectando con servidor")
                    bdFree[key] = -1
                    del servers[serverID]
        time.sleep(120)

def serversThread(client, addr):
    global bdFree
    global servers
    data = client.recv(1024).decode()
    if 'register' in data.lower():
        ipport = data.lower().replace("register", "").strip().split(",")
        
        
        with open('sqlcmds.json', 'r') as f:
            sqlcmds = json.load(f)
        if sqlcmds:
            lastCMD = list(sqlcmds.keys())[-1]
            res = "Registration succesfull. Prepare to recive database. cmds:" + lastCMD
            client.send(res.encode())
            for key in sqlcmds.keys():
                cmd = sqlcmds[key]
                client.send(cmd.encode())
                client.recv(1024)
            response = client.recv(1024).decode()
            if "ready" in response:
                if servers:
                    lastID = max(servers.keys())
                    clientsperServer[lastID+1] = 0
                    servers[lastID+1] = (addr[0], int(ipport[0]), addr[0], int(ipport[1]))
                else:
                    clientsperServer[1] = 0
                    servers[1] = (addr[0], int(ipport[0]), addr[0], int(ipport[1]))
                print("server registered")
        else:
            
            if servers:
                lastID = max(servers.keys())
                clientsperServer[lastID+1] = 0
                servers[lastID+1] = (addr[0], int(ipport[0]), addr[0], int(ipport[1]))
            else:
                clientsperServer[1] = 0
                servers[1] = (addr[0], int(ipport[0]), addr[0], int(ipport[1]))
            print("server registered")
            res = "Registration succesfull."
            client.send(res.encode())
        
    elif "request" in data.lower():
        request = data.lower().replace("request db", "").strip().split(",")
        port = int(request[0])
        resource = request[1]
        serverID = searchServerID(addr[0], port)
        if serverID in bdFree.values():
            res =  "BD busy"
        else:
            if resource not in bdFree.keys():
                if serverID != -1:
                    res = "BD free"
                    bdFree[resource] = serverID
                else:
                    res = "BD busy"
            else:
                if bdFree[resource] == -1:
                    if serverID != -1:
                        res = "BD free"
                        bdFree[resource] = serverID
                    else:
                        res = "BD busy"
                else:
                    res = "BD busy"
        client.send(res.encode())
    elif  "release" in data.lower():
        resource = data.lower().replace("release db ", "").strip()
        sqlcmds = {}
        with open('sqlcmds.json') as f:
            sqlcmds = json.load(f)
        nextIndex = 1
        if sqlcmds:
            nextIndex = int(list(sqlcmds.keys())[-1]) + 1
        res = "sendCommands"
        client.send(res.encode())
        count = int(client.recv(1024).decode())
        reads = 0
        newCMDs = {}
        while reads < count:
            data = client.recv(1024)
            cmd = data.decode()
            newCMDs[str(reads+1)] = cmd
            sqlcmds[str(nextIndex)] = cmd
            client.send("ok".encode())
            nextIndex += 1
            reads += 1
            
        notifyServers(newCMDs, resource)
        with open('sqlcmds.json', 'w') as f:
            json.dump(sqlcmds,f)
        bdFree[resource] = -1
    client.close()
         
def notifyServers(sqlcmds, resource):
    global servers
    for serverID in servers.keys():
        if serverID != bdFree[resource]:
            try:
                ipport = servers[serverID]
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((ipport[2], ipport[3]))
                #print('Connected')
                lastCMD = str(list(sqlcmds.keys())[-1])
                data = "actualizar db. cmds:" + lastCMD
                s.send(data.encode())
                for key in sqlcmds.keys():
                    s.recv(1024)
                    cmd = sqlcmds[key]
                    s.send(cmd.encode())
                    
                s.close()
            except:
                #print("error conectando con servidor")
                del servers[serverID]



def checkClients():
    global clientsperServer
    while True:
        try:
            for serverID in servers.keys():
                ipport = servers[serverID]
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                try:
                    s.connect((ipport[2], ipport[3]))
                    data = "get clients"
                    s.send(data.encode())
                    data = s.recv(1024)
                    clients = int(data.decode())
                    clientsperServer[serverID] = clients
                    s.close()
                except:
                    del servers[serverID]
                    del clientsperServer[serverID]
        except:
            continue
        time.sleep(30)  

def checkserver(ipport):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ipport[2], ipport[3]))
        s.send('check'.encode())
        data = s.recv(1024).decode
        s.close()
        return True
    except:
        return False
    

def clientsThread(client):
    data = client.recv(1024).decode()
    
    if "server" in data:
        find = False
        while not find:
            letters = string.ascii_lowercase
            randomkey = ''.join(random.choice(letters) for i in range(10))
            if len(randomkey) ==0:
                randomkey = 'random'
            token = TokenGenerator.getAuthenticationToken(randomkey)
            key_min = min(clientsperServer.keys(), key=(lambda k: clientsperServer[k]))
            ipport = servers[key_min]
            if checkserver(ipport):
                st = ipport[0] + ":" + str(ipport[1]) + " " + token
                client.send(st.encode())
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((ipport[2], ipport[3]))
                s.send(token.encode())
                s.close()
                find = True
            else:
                del servers[key_min]

def main():
    host = ''
    port = int(input('Ingrese el puerto para atender clientes: '))
    hostWithServers = ''
    portWithServers = int(input('Ingrese el puerto para atender servidores: '))
    f = open('sqlcmds.json', 'w')
    f.write("{}")
    f.close()
    x = threading.Thread(target=atendServers, args=(hostWithServers,portWithServers,))
    x.start()

    y = threading.Thread(target=checkClients)
    y.start()

    #inicio de server
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen(5)
    while True:
        c, addr = s.accept()
        t = threading.Thread(target=clientsThread, args=(c,))
        t.start()



if __name__ == "__main__":
    main()
    pass