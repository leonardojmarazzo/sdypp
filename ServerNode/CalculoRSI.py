import numpy as np
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import mplfinance as mpf
import pandas as pd
import sqliteconection as sqlitecon
import datetime
import random
import string


def SMA(series):
    SUM = 0
    for n in series:
        SUM += n
    return SUM / len(series)


def CALCRSI(series, dates, periodo):
    difpos = {}
    difneg = {}
    RSI = {}
    for i in range(1, len(series)):
        diff = round(series[i] - series[i-1], 2)
        if diff > 0:
            difpos[dates[i]] = diff
            difneg[dates[i]] = 0
        else:
            difneg[dates[i]] = abs(diff)
            difpos[dates[i]] = 0
    i = 0
    SUMPOS = 0
    SUMNEG = 0
    AVGPOS = 0
    AVGNEG = 0
    for fecha in difpos:
        if (i < periodo):
            SUMPOS += difpos[fecha]
            SUMNEG += difneg[fecha]
        else:
            if (AVGPOS == 0):
                AVGPOS = SUMPOS / periodo
                AVGNEG = SUMNEG / periodo
            else:
                AVGPOS = (AVGPOS * (periodo - 1) + difpos[fecha])/periodo
                AVGNEG = (AVGNEG * (periodo - 1) + difneg[fecha])/periodo
            if (AVGNEG == 0):
                RS = 99
            else:
                RS = AVGPOS / AVGNEG
            RSIA = 100 - (100 / (RS + 1))
            RSI[fecha] = RSIA
        i += 1
    return RSI


def calculoRSI(symbol, periodo=0):
    
    try:
        letters = string.ascii_lowercase
        randomname = ''.join(random.choice(letters) for i in range(15))
        filename = symbol + "-" + randomname + '.png'
        cnxn = sqlitecon.sql_connection()

        now = datetime.datetime.now()
        hoy = now.strftime("%y-%m-%d")

        cursor = cnxn.cursor()
        if periodo == 0:
            sql_cmd = "Select * from Periodos where symbolo like '%" + symbol + "%'"
            cursor.execute(sql_cmd)
            res = cursor.fetchone()
            if res:
                periodo = res[1]
            else:
                return 'No data for Symbol'

        sql_cmd = "Select * from Simbolos where simbolo = '" + symbol + "'"
        cursor.execute(sql_cmd)
        res = cursor.fetchall()
        dates = []
        series = []
        if res:
            for row in res:
                dates.append(row[1])
                series.append(row[5])
        else:
            return 'No data for Symbol'

        RSI = CALCRSI(series, dates, periodo)

        sql_cmd = "Select Fecha,Apertura,High,Low,Cierre from (Select Fecha,Apertura,High,Low,Cierre from Simbolos where Simbolo = '{}' ORDER BY Fecha DESC LIMIT 15) ORDER BY Fecha ASC".format(
            symbol)
        df = pd.read_sql_query(sql_cmd, cnxn)
        df.rename(columns={'Fecha': 'Date', 'Apertura': 'Open',
                        'Cierre': 'Close'}, inplace=True)
        df['Date'] = pd.to_datetime(df['Date'])
        df.set_index("Date", inplace=True,)

        y = list(RSI.values())[-15:]
        RSIMIN = pd.DataFrame({'RSIMIN': np.full(15,30).tolist()})
        RSIMAX = pd.DataFrame({'RSIMAX': np.full(15,70).tolist()})
        dfRSI = pd.DataFrame({'RSI': y})
        apd = [mpf.make_addplot(dfRSI, panel='lower'),
        mpf.make_addplot(RSIMIN, panel='lower'),
        mpf.make_addplot(RSIMAX, panel='lower')]
        mpf.plot(df, type='candle', addplot=apd, savefig=dict(
                fname=filename, dpi=300, pad_inches=0.50))
        cursor.close()
        cnxn.close()
        return filename
    except:
        return 'Error en el calculo de RSI'
