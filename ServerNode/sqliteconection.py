import sqlite3
from sqlite3 import Error


def sql_connection():

    try:

        con = sqlite3.connect('Acciones.db')

        return con

    except Error:

        print(Error)


def initializate(con):

    cursorObj = con.cursor()

    cursorObj.execute(
        "CREATE TABLE Medias (symbolo text PRIMARY KEY, MMC int, MML int)")
    cursorObj.execute(
        "CREATE TABLE Periodos (symbolo text PRIMARY KEY, Periodo int)")
    cursorObj.execute('CREATE TABLE Simbolos ("Simbolo"	TEXT,"Fecha" TEXT,"Apertura" REAL NOT NULL,"High" REAL NOT NULL, "Low"	REAL NOT NULL, "Cierre"	REAL NOT NULL, PRIMARY KEY("Simbolo","Fecha"))')

    con.commit()
