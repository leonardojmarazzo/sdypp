import numpy as np
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import mplfinance as mpf
import pandas as pd
import datetime
import sys
import sqliteconection as sqlitecon
import traceback
import random
import string

def SMA(series):
    SUM = 0
    for n in series:
        SUM += n
    return SUM / len(series)


def EMA(series, periodo, dates):
    emas = {}
    if len(series) >= periodo:

        EMAANT = SMA(series[0:periodo])
        emas[dates[periodo-1]] = EMAANT
        for i in range(periodo, len(series)):
            EMAAC = series[i]*(2/(periodo + 1)) + EMAANT * \
                (1 - (2/(periodo + 1)))
            emas[dates[i]] = EMAAC
            EMAANT = EMAAC
    return emas


def calcularMCO(symbol, TPC = 0,TPL = 0):
    try:
        letters = string.ascii_lowercase
        randomname = ''.join(random.choice(letters) for i in range(15))
        filename = symbol + "-" + randomname + '.png'
        cnxn = sqlitecon.sql_connection()
        

        now = datetime.datetime.now()
        hoy = now.strftime("%y-%m-%d")

        fig_size = plt.rcParams["figure.figsize"]
        fig_size[0] = 30
        fig_size[1] = 15
        plt.rcParams["figure.figsize"] = fig_size
        cursor = cnxn.cursor()
        if TPC == 0 or TPL == 0:
            sql_cmd = "Select * from Medias where Symbolo = '" + symbol + "'"
            cursor.execute(sql_cmd)
            res = cursor.fetchone()
            if res:
                if TPC == 0:
                    TPC = res[1]
                if TPL == 0:
                    TPL = res[2]
            else:
                return 'No data for symbol'

        sql_cmd = "Select * from Simbolos where simbolo = '" + symbol + "'"
        cursor.execute(sql_cmd)
        res = cursor.fetchall()
        
        dates = []
        series = []
        if res:
            for row in res:
                dates.append(row[1])
                series.append(row[5])
        else:
            return 'No data for symbol'

        em = EMA(series, TPC, dates)

        eml = EMA(series, TPL, dates)
        sql_cmd = "Select Fecha,Apertura,High,Low,Cierre from (Select Fecha,Apertura,High,Low,Cierre from Simbolos where Simbolo = '{}' ORDER BY Fecha DESC LIMIT 15) ORDER BY Fecha ASC".format(
            symbol)
        df = pd.read_sql_query(sql_cmd, cnxn)
        df.rename(columns={'Fecha': 'Date', 'Apertura': 'Open',
                           'Cierre': 'Close'}, inplace=True)
        df['Date'] = pd.to_datetime(df['Date'])
        df.set_index("Date", inplace=True)

        y = list(em.values())[-15:]
        y2 = list(eml.values())[-15:]
        dfMM = pd.DataFrame({'MMC': y, 'MML': y2})
        apd = mpf.make_addplot(dfMM)
        mpf.plot(df, type='candle', addplot=apd, savefig=dict(
            fname=filename, dpi=300, pad_inches=0.50))
        cursor.close()
        cnxn.close()
        return filename
    except:
        return 'Error en calculo del indicador'

if __name__ == "__main__":
    calcularMCO('HAVA', 2, 21)
    pass
