import socket
import threading
import Scraping
import SimulacionRSI
import Simulacion
import os
import TokenGenerator
import Calculo
import CalculoRSI
import EditarRSI
import EditarMM
import os.path
from os import path
import sqliteconection as sqlitecon
import json
import time


clientsCount = 0
presharedKey = "editionPassword"
hashes = []
IPBal = ''
portBal = 0
portWithBal = 0
port = 0
registration = False

def calcularHash():
    st = TokenGenerator.getAuthenticationToken(presharedKey)
    return st


def comunicationWithBalancer(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen(5)
    print("socket is listening")
    global hashes
    while True:
        c, addr = s.accept()
        data = c.recv(1024).decode()
        if 'clients' in data:
            res = str(clientsCount).encode()
            c.send(res)
        elif 'actualizar' in data:
            totalCMDs = int(data.split(":")[1])
            cmdsCount = 0
            cmds = {}
            while cmdsCount < totalCMDs:
                c.send("ok".encode())
                data = c.recv(1024).decode()
                cmds[str(cmdsCount)] = data
                cmdsCount += 1
            while registration:
                time.sleep(180)
            actualizarDB(cmds)
        elif 'check' in data:
            res = 'alive'.encode()
            c.send(res)
            c.close()
        else:
            hashes.append(data)


def serverThread(client):
    global clientsCount
    global hashes
    clientsCount += 1
    data = client.recv(1024)
    token = data.decode().strip("\r\n")
    if token in hashes:
        index = hashes.index(token)
        hashes[index] = ""
        default(client)
        data = client.recv(1024)
        while data:
            comando = data.decode().replace("\n", "")
            if "ACTUALIZAR DATOS" in comando.upper():
                actualizarDatos(client, comando)
            elif "SIMULACION" in comando.upper():
                if "RSI" in comando.upper():
                    simulacionRSI(client, comando)
                else:
                    simulacionMM(client, comando)
            elif "EDITAR" in comando.upper():
                if 'RSI' in comando.upper():
                    editarRSI(client, comando)
                elif 'MCO' in comando.upper():
                    editarMCO(client, comando)
            elif "CALCULO" in comando.upper():
                if "RSI" in comando.upper():
                    calculoRSI(client, comando)
                elif 'MCO' in comando.upper():
                    calculoMCO(client, comando)
            else:
                default(client)
            data = client.recv(1024)
    else:
        res = "Token invalido"
        client.send(res.encode())
        client.close()
    clientsCount -= 1


def solicitarDB(resource):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((IPBal, portBal))
    st = "request db {},{}".format(port,resource)
    s.send(st.encode())
    data = s.recv(1024).decode()
    s.close()
    if 'busy' in data:
        return False
    else:
        return True


def liberarBD(resource):
    cmds = []
    f = open("SQLCMDS.txt", 'r')
    lines = f.readlines()
    for line in lines:
        cmds.append(line.strip("\n"))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((IPBal, portBal))
    st = "release db {}".format(resource)
    s.send(st.encode())
    data = s.recv(1024).decode()
    if 'send' in data:
        count = str(len(cmds))
        s.send(count.encode())
        for cmd in cmds:
            s.send(cmd.encode())
            s.recv(1024)
    s.close()


def editarRSI(client, comando):
    response = "Envie el token"
    client.send(response.encode())
    data = client.recv(1024).decode()
    if calcularHash() == data:
        comando = str(comando).upper().replace("EDITAR RSI", "").strip()
        listParams = comando.split()
        simbolo = listParams[0]
        periodo = listParams[1]
        if solicitarDB(simbolo):
            
            res = EditarRSI.EditarRSI(simbolo, periodo)
            if 'Exito' in res:
                response = "Actuaizacion exitosa"
                client.send(response.encode())
            else:
                response = "Error en actualizacion"
                client.send(response.encode())
            liberarBD(simbolo)
        else:
            response = "Base de datos ocupada intente mas tarde"
            client.send(response.encode())
    else:
        response = "Token incorrecto"
        client.send(response.encode())


def editarMCO(client, comando):
    response = "Envie el token"
    client.send(response.encode())
    data = client.recv(1024).decode()
    if calcularHash() == data:
        comando = str(comando).upper().replace("EDITAR MCO", "").strip()
        listParams = comando.split()
        simbolo = listParams[0]
        MMC = listParams[1]
        MML = listParams[2]
        if solicitarDB(simbolo):
            res = EditarMM.editar(simbolo, MMC, MML)
            if 'Exito' in res:
                response = "Actuaizacion exitosa"
                client.send(response.encode())
            else:
                response = "Error en actualizacion"
                client.send(response.encode())
            liberarBD(simbolo)
        else:
            response = "Base de datos ocupada intente mas tarde"
            client.send(response.encode())
    else:
        response = "Token incorrecto"
        client.send(response.encode())


def calculoRSI(client, comando):
    comando = str(comando).upper().replace("CALCULO RSI", "").strip()
    listParams = comando.split()
    simbolo = listParams[0]
    if len(listParams) == 2:
        periodo = int(listParams[1])
        filename = CalculoRSI.calculoRSI(simbolo, periodo=periodo)
    else:
        filename = CalculoRSI.calculoRSI(simbolo)

    if simbolo in filename:
        filesize = str(os.path.getsize(filename))
        response = "Prepare to recive file. Size:"+filesize
        client.send(response.encode())
        client.recv(1024)
        f = open(filename, 'rb')
        l = f.read(1024)
        while l:
            client.send(l)
            l = f.read(1024)
        f.close()
        os.remove(filename)
    elif 'error' in filename.lower():
        response = "Error en el calculo. Intente nuevamente"
        client.send(response.encode())
    elif 'data' in filename.lower():
        response = "No hay datos para ese activo"
        client.send(response.encode())


def calculoMCO(client, comando):
    comando = str(comando).upper().replace("CALCULO MCO", "").strip()
    listParams = comando.split()
    simbolo = listParams[0]
    filename = ""
    if len(listParams) == 2:
        if 'TPC' in listParams[1]:
            TPC = int(listParams[1].replace('TPC:', ''))
            filename = Calculo.calcularMCO(simbolo, TPC=TPC)
        else:
            TPL = int(listParams[1].replace('TPL:', ''))
            filename = Calculo.calcularMCO(simbolo, TPL=TPL)
    elif len(listParams) == 3:
        if 'TPC' in listParams[1]:
            TPC = int(listParams[1].replace('TPC:', ''))
            TPL = int(listParams[2].replace('TPL:', ''))
        else:
            TPL = int(listParams[1].replace('TPL:', ''))
            TPC = int(listParams[2].replace('TPC:', ''))
        filename = Calculo.calcularMCO(simbolo, TPC=TPC, TPL=TPL)
    else:
        filename = Calculo.calcularMCO(simbolo)
    if simbolo in filename:
        filesize = str(os.path.getsize(filename))
        response = "Prepare to recive file. Size:"+filesize
        client.send(response.encode())
        client.recv(1024)
        f = open(filename, 'rb')
        l = f.read(1024)
        while l:
            client.send(l)
            l = f.read(1024)
        f.close()
        os.remove(filename)
    elif 'error' in filename.lower():
        response = "Error en el calculo. Intente nuevamente"
        client.send(response.encode())
    elif 'data' in filename.lower():
        response = "No hay datos para ese activo"
        client.send(response.encode())


def simulacionRSI(client, comando):
    comando = str(comando).upper().replace("SIMULACION RSI", "").strip()
    listParams = comando.split()
    simbolo = listParams[0]
    valsrango = listParams[1].split(",")
    rango = range(int(valsrango[0]), int(valsrango[1])+1)
    res = SimulacionRSI.simulacionRSI(simbolo, rango)
    if simbolo in res:
        filesize = str(os.path.getsize(res))
        response = "Prepare to recive file. Size:"+filesize
        client.send(response.encode())
        client.recv(1024)
        f = open(res, 'rb')
        l = f.read(1024)
        while l:
            client.send(l)
            l = f.read(1024)
            c.recv(1024)
        f.close()
        os.remove(res)
    elif 'error' in res.lower():
        response = "Error en la simulacion. Intente nuevamente"
        client.send(response.encode())
    elif 'data' in res.lower():
        response = "No hay datos para ese activo"
        client.send(response.encode())


def simulacionMM(client, comando):
    comando = str(comando).upper().replace("SIMULACION MCO", "").strip()
    listParams = comando.split()
    simbolo = listParams[0]
    valsrango = listParams[1].split(",")
    valsrango2 = listParams[2].split(",")
    rango1 = range(int(valsrango[0]), int(valsrango[1])+1)
    rango2 = range(int(valsrango2[0]), int(valsrango2[1])+1)
    res = Simulacion.SimulacionMM(simbolo, rango1, rango2)

    if simbolo in res:
        filesize = str(os.path.getsize(res))
        response = "Prepare to recive file. Size:"+filesize
        client.send(response.encode())
        client.recv(1024)
        f = open(res, 'rb')
        l = f.read(1024)
        while l:
            client.send(l)
            l = f.read(1024)
            client.recv(1024)
        f.close()
        os.remove(res)
    elif 'error' in res.lower():
        response = "Error en la simulacion. Intente nuevamente"
        client.send(response.encode())
    elif 'data' in res.lower():
        response = "No hay datos para ese activo"
        client.send(response.encode())


def actualizarDatos(client, comando):
    response = "Envie el token"
    client.send(response.encode())
    data = client.recv(1024).decode()
    if calcularHash() == data:
        simbolo = str(comando).upper().replace(
                "ACTUALIZAR DATOS", "").strip()
        if solicitarDB(simbolo):
            res = Scraping.Scraping(simbolo)
            if simbolo in res:
                filesize = str(os.path.getsize(res))
                response = "Prepare to recive file. Size:"+filesize
                client.send(response.encode())
                client.recv(1024)
                f = open(res, 'rb')
                l = f.read(1024)
                while l:
                    client.send(l)
                    l = f.read(1024)
                    client.recv(1024)
                f.close()
                print('done sending file')
                os.remove(res)
            elif 'error' in res.lower():
                response = "Error en update. Intente nuevamente"
                client.send(response.encode())
            liberarBD(simbolo)
        else:
            response = "Base de datos ocupada intente mas tarde"
            client.send(response.encode())
    else:
        response = "Token incorrecto"
        client.send(response.encode())


def default(client):
    lista = []
    lista.append("{:25}{:50}".format("Comandos disponibles", "Formato"))
    lista.append("{:25}{:50}".format(
        "Actualizar datos", "Actualizar Datos [activo]"))
    lista.append("{:25}{:50}".format(
        "Simulacion", "Simulacion [indicador] [activo] [rangos parametros]"))
    lista.append("{:25}{:50}".format(
        "Editar", "Editar [indicador] [activo] [valores separados por coma]"))
    lista.append("{:25}{:50}".format(
        "Calculo", "Calculo [indicador] [activo] [parametros (opcional)"))
    response = "\n".join(lista) + "\n"
    client.send(response.encode())


def registerToBalancer(IPBal, portBal, port, portWithBAl):
    sqlcmds = ""
    if path.exists("Acciones.db"):
        os.remove("Acciones.db")
    sqlitecon.initializate(sqlitecon.sql_connection())
    print("registering")
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((IPBal, portBal))
    st = "register " + str(port) + "," + str(portWithBAl)
    s.send(st.encode())
    data = s.recv(1024).decode()
    #print(data)
    if 'database' in data:
        totalCMDs = int(data.split(":")[1])
        cmdsCount = 0
        cmds = {}
        while cmdsCount < totalCMDs:
            data = s.recv(1024)
            cmds[str(cmdsCount)] = data.decode()
            cmdsCount += 1
            res = "ok"
            s.send(res.encode())
            #print(cmdsCount)
        actualizarDB(cmds)
        s.send("Server ready".encode())
        s.close()
    else:
        s.close()


def actualizarDB(cmds):
    print("Actualizando DB")
    lastindex = int(list(cmds.keys())[-1])
    cnxn = sqlitecon.sql_connection()
    for i in range(1, lastindex+1):
        sqlcmd = cmds[str(i)]
        cursor = cnxn.cursor()
        cursor.execute(sqlcmd)
        #print(sqlcmd)
        cnxn.commit()


def main():
    global IPBal
    global portBal
    global portWithBAl
    global port
    global registration
    IPBal = input("Ingrese la IP del balanceador: ")
    portBal = int(input("Ingrese el puerto del balanceador: "))
    host = ''
    port = int(input('Ingrese el puerto para atender clientes: '))
    hostWithBal = ''
    portWithBAl = int(input('Ingrese el puerto para atender al balanceador: '))
    x = threading.Thread(target=comunicationWithBalancer,
                         args=(hostWithBal, portWithBAl,))
    x.start()

    # inicio de server
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    registration = True
    registerToBalancer(IPBal, portBal, port, portWithBAl)
    registration = False
    # put the socket into listening mode
    s.listen(5)
    print("socket is listening")
    # a forever loop until client wants to exit
    while True:
        # establish connection with client
        c, addr = s.accept()
        print('Connected to :', addr[0], ':', addr[1])
        t = threading.Thread(target=serverThread, args=(c,))
        t.start()


if __name__ == "__main__":
    main()
    pass
