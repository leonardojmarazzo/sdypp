import pyodbc
import numpy as np
import datetime
import matplotlib.pyplot as plt
import pandas as pd
import sqliteconection as sqlitecon
import random
import string

def CALCRSI(series, periodo, dates):
    difpos = {}
    difneg = {}
    RSI = {}
    for i in range(1, len(series)):
        diff = round(series[i] - series[i-1], 2)
        if diff > 0:
            difpos[dates[i]] = diff
            difneg[dates[i]] = 0
        else:
            difneg[dates[i]] = abs(diff)
            difpos[dates[i]] = 0
    i = 0
    SUMPOS = 0
    SUMNEG = 0
    AVGPOS = 0
    AVGNEG = 0
    for fecha in difpos:
        if (i < periodo):
            SUMPOS += difpos[fecha]
            SUMNEG += difneg[fecha]
        else:
            if (AVGPOS == 0):
                AVGPOS = SUMPOS / periodo
                AVGNEG = SUMNEG / periodo   
            else:
                AVGPOS = (AVGPOS * (periodo - 1) + difpos[fecha])/periodo
                AVGNEG = (AVGNEG * (periodo - 1) + difneg[fecha])/periodo
            if (AVGNEG == 0):
                RS = 99
            else:
                RS = AVGPOS / AVGNEG
            RSIA = 100 - (100 / (RS + 1))
            RSI[fecha] = RSIA
        i += 1
    return RSI


def simulacionRSI(symbol, r):
    try:
        cnxn = sqlitecon.sql_connection()
        cnxn = sqlitecon.sql_connection()
        letters = string.ascii_lowercase
        randomname = ''.join(random.choice(letters) for i in range(15))
        filename = symbol + "-" + randomname + '.csv'

        print("Simulacion {}".format(symbol))
        a = open(filename, "w")
        a.write("Periodo;Trades Totales;Eficacia total;Eficacia promedio;Promedio Eficacias;Promedio Duracion;Trades Positivos;Trades Negativos;Probabilidad de eficacia \n")
        a.close()
        for pc in r:
                DineroIncial = 10000
                DineroDisponible = 10000
                OperacionAbierta = False
                DineroInvertido = 0
                CantidadComprada = 0
                DineroFinal = 0
                ContadorTrades = 0
                ContadorTradesPositivos = 0
                ContadorTradesNegativos = 0
                SUMEficaciaOperacion = 0
                SUMDiasOperacion = 0

                cursor = cnxn.cursor()
                sql_cmd = "Select * from Simbolos where simbolo = '" + symbol + "'"
                cursor.execute(sql_cmd)
                res = cursor.fetchall()
                if res:
                    dates = []
                    series = []
                    seriesH = []
                    for row in res:
                        dates.append(row[1])
                        seriesH.append(row[3])
                        series.append(row[5])
                else:
                    return 'No data for the symbol'
                
                RSI = CALCRSI(series, pc, dates)
                inicio = 0
                if (len(series) > (1000 + pc)):
                    inicio = len(series) - 1000
                else:
                    inicio = pc + 1
                fechaApertura = datetime.datetime.today()
                for i in range(inicio, len(series) - 1):
                        fecha = dates[i]
                        date = datetime.datetime.strptime(fecha, "%Y-%m-%d")
                        RSIf = RSI[fecha]
                        precio = seriesH[i]
                        #abrir posicion
                        if ((RSIf < 30) and (not OperacionAbierta)):
                            fechaApertura = date
                            CantidadComprada = DineroDisponible // precio
                            DineroInvertido = CantidadComprada * precio
                            DineroDisponible = DineroDisponible - DineroInvertido
                            OperacionAbierta = True

                        #cerrar posicion
                        if ((RSIf > 70) and OperacionAbierta):
                            DiasOperacion = (date - fechaApertura).days
                            SUMDiasOperacion = SUMDiasOperacion + DiasOperacion
                            DineroVenta = CantidadComprada * precio
                            if (DineroInvertido < DineroVenta):
                                ContadorTradesPositivos = ContadorTradesPositivos + 1
                            else:
                                ContadorTradesNegativos = ContadorTradesNegativos + 1
                            EficaciaOperacion = (DineroVenta - DineroInvertido) / DineroInvertido
                            SUMEficaciaOperacion = SUMEficaciaOperacion + EficaciaOperacion
                            DineroDisponible = DineroDisponible + DineroVenta
                            DineroInvertido = 0
                            CantidadComprada = 0
                            OperacionAbierta = False
                            ContadorTrades = ContadorTrades + 1
                            
                if (OperacionAbierta):
                    fecha = dates[-1]
                    precio = seriesH[-1]
                    DineroVenta = CantidadComprada * precio
                    if (DineroInvertido < DineroVenta):
                        ContadorTradesPositivos = ContadorTradesPositivos + 1
                    else:
                        ContadorTradesNegativos = ContadorTradesNegativos + 1
                    EficaciaOperacion = (
                        DineroVenta - DineroInvertido) / DineroInvertido
                    SUMEficaciaOperacion = SUMEficaciaOperacion + EficaciaOperacion
                    DineroDisponible = DineroDisponible + DineroVenta
                    DineroInvertido = 0
                    CantidadComprada = 0
                    ContadorTrades = ContadorTrades + 1
                    OperacionAbierta = False
                    
                if (ContadorTrades != 0):
                    DineroFinal = DineroDisponible
                    EficiaTotal = (DineroFinal - DineroIncial) / DineroIncial
                    EficaciaPromedio = EficiaTotal / ContadorTrades
                    PromedioEficacia = SUMEficaciaOperacion / ContadorTrades
                    ProbabilidadEficacia = ContadorTradesPositivos / ContadorTrades
                    PromedioDuracion = int(SUMDiasOperacion / ContadorTrades)
                    a = open(filename, "a")
                    a.write(str(pc) + ";" + str(ContadorTrades) + ";" + str(EficiaTotal * 100).replace(".",",")  + "%;" + str(EficaciaPromedio * 100).replace(".",",") + "%;" +
                            str(PromedioEficacia * 100).replace(".",",")  + "%;" + str(PromedioDuracion) + ";" + str(ContadorTradesPositivos) + ";" + str(ContadorTradesNegativos) + ";" + str(ProbabilidadEficacia * 100).replace(".",",") + "%\n")
                    a.close()
                cursor.close()

        cnxn.close()
        return filename
    except:
        return "Error en la simulacion"
