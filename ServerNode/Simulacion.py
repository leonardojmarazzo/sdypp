import pyodbc
import numpy as np
import datetime
import matplotlib.pyplot as plt
import pandas as pd
import sqliteconection as sqlitecon
import string
import random

def SMA(series):
    SUM = 0
    for n in series:
        SUM += n
    return SUM / len(series)


def EMA(series, periodo, dates):
    emas = {}
    if len(series) >= periodo:
        EMAANT = SMA(series[0:periodo])
        emas[dates[periodo - 1]] = EMAANT
        for i in range(periodo, len(series)):
            EMAAC = series[i]*(2/(periodo + 1)) + \
                EMAANT * (1 - (2/(periodo + 1)))
            emas[dates[i]] = EMAAC
            EMAANT = EMAAC
    return emas


def SimulacionMM(symbol, range1, range2):
    cnxn = sqlitecon.sql_connection()
    letters = string.ascii_lowercase
    randomname = ''.join(random.choice(letters) for i in range(15))
    filename = symbol + "-" + randomname + '.csv'
    print("Simulacion {}".format(symbol))
    a = open(filename, "w")
    a.write("MMC;MML;Trades Totales;Eficacia total;Eficacia promedio;Promedio Eficacias;Promedio Duracion;Trades Positivos;Trades Negativos;Probabilidad de eficacia \n")
    a.close()
    for pc in range1:
        for pl in range2:
            DineroIncial = 10000
            DineroDisponible = 10000
            OperacionAbierta = False
            DineroInvertido = 0
            CantidadComprada = 0
            DineroFinal = 0
            ContadorTrades = 0
            ContadorTradesPositivos = 0
            ContadorTradesNegativos = 0
            SUMEficaciaOperacion = 0
            SUMDiasOperacion = 0

            cursor = cnxn.cursor()
            sql_cmd = "Select * from Simbolos where simbolo = '" + symbol + "'"
            cursor.execute(sql_cmd)
            res = cursor.fetchall()

            if res:
                dates = []
                series = []
                seriesH = []
                for row in res:
                    dates.append(row[1])
                    seriesH.append(row[3])
                    series.append(row[5])
            else:
                return 'No data for the symbol'

            emasC = EMA(series, pc, dates)
            emasL = EMA(series, pl, dates)
            inicio = 0
            if (len(series) > (1000 + pl)):
                inicio = len(series) - 1000
            else:
                inicio = pl
            fechaApertura = datetime.datetime.today()
            for i in range(inicio, len(series) - 1):
                fecha = dates[i]
                date = datetime.datetime.strptime(fecha, "%Y-%m-%d")
                emac = emasC[fecha]
                emal = emasL[fecha]
                precio = seriesH[i]
                if (precio == 0):
                    precio = series[i]

                # abrir posicion
                if ((emac > emal) and (not OperacionAbierta)):
                    fechaApertura = date
                    CantidadComprada = DineroDisponible // precio
                    DineroInvertido = CantidadComprada * precio
                    DineroDisponible = DineroDisponible - DineroInvertido
                    OperacionAbierta = True

                # cerrar posicion
                if ((emac < emal) and OperacionAbierta):
                    DiasOperacion = (date - fechaApertura).days
                    SUMDiasOperacion = SUMDiasOperacion + DiasOperacion
                    DineroVenta = CantidadComprada * precio
                    if (DineroInvertido < DineroVenta):
                        ContadorTradesPositivos = ContadorTradesPositivos + 1
                    else:
                        ContadorTradesNegativos = ContadorTradesNegativos + 1
                    EficaciaOperacion = (
                        DineroVenta - DineroInvertido) / DineroInvertido
                    SUMEficaciaOperacion = SUMEficaciaOperacion + EficaciaOperacion
                    DineroDisponible = DineroDisponible + DineroVenta
                    DineroInvertido = 0
                    CantidadComprada = 0
                    ContadorTrades = ContadorTrades + 1
                    OperacionAbierta = False
            if (OperacionAbierta):
                fecha = dates[-1]
                emac = emasC[fecha]
                emal = emasL[fecha]
                precio = seriesH[-1]
                DineroVenta = CantidadComprada * precio
                if (DineroInvertido < DineroVenta):
                    ContadorTradesPositivos = ContadorTradesPositivos + 1
                else:
                    ContadorTradesNegativos = ContadorTradesNegativos + 1
                EficaciaOperacion = (
                    DineroVenta - DineroInvertido) / DineroInvertido
                SUMEficaciaOperacion = SUMEficaciaOperacion + EficaciaOperacion
                DineroDisponible = DineroDisponible + DineroVenta
                DineroInvertido = 0
                CantidadComprada = 0
                ContadorTrades = ContadorTrades + 1
                OperacionAbierta = False
            DineroFinal = DineroDisponible
            EficiaTotal = (DineroFinal - DineroIncial) / DineroIncial
            EficaciaPromedio = EficiaTotal / ContadorTrades
            PromedioEficacia = SUMEficaciaOperacion / ContadorTrades
            ProbabilidadEficacia = ContadorTradesPositivos / ContadorTrades
            PromedioDuracion = int(SUMDiasOperacion / ContadorTrades)
            a = open(filename, "a")
            a.write(str(pc) + ";" + str(pl) + ";" + str(ContadorTrades) + ";" + str(EficiaTotal * 100).replace(".", ",") + "%;" + str(EficaciaPromedio * 100).replace(".", ",") + "%;" +
                    str(PromedioEficacia * 100).replace(".", ",") + "%;" + str(PromedioDuracion) + ";" + str(ContadorTradesPositivos) + ";" + str(ContadorTradesNegativos) + ";" + str(ProbabilidadEficacia * 100).replace(".", ",") + "%\n")
            a.close()
            cursor.close()
    cnxn.close()
    return filename
