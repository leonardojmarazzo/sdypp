import http.client
import json
import datetime
import time
import calendar
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import mplfinance as mpf
import sqliteconection as sqlitecon
import random
import string
import traceback
import pandas as pd


def Scraping(symbol):
    try:
        cmds = []
        now = datetime.datetime.now()
        cnxn = sqlitecon.sql_connection()
        comandsFile = 'SQLCMDS.txt'
        f = open(comandsFile, 'w')
        f.close()

        print(symbol)
        resolution = "D"
        fromDate = 0
        cursor = cnxn.cursor()
        sql_cmd = "Select MAX(Fecha) from simbolos where simbolo = '" + \
            symbol + "'"
        cursor.execute(sql_cmd)
        res = cursor.fetchone()

        if res[0] == None:
            fromDate = 0
        else:
            s = res[0]
            fromDate = int(calendar.timegm(
                datetime.datetime.strptime(s, "%Y-%m-%d").timetuple()))
        if (now.hour > 17):
            ayer = datetime.datetime.today() + datetime.timedelta(days=1)
        else:
            ayer = datetime.datetime.today()
        s = ayer.strftime("%d/%m/%Y")
        toDate = int(calendar.timegm(
            datetime.datetime.strptime(s, "%d/%m/%Y").timetuple()))

        conn = http.client.HTTPSConnection("www.invertironline.com")
        args = "/api/cotizaciones/history?symbolName=" + symbol + "&exchange=BCBA&from=" + \
            str(fromDate)+"&to="+str(toDate)+"&resolution="+resolution
        conn.request("GET", args, "", {})

        res = conn.getresponse()
        data = res.read()
        conn.close()
        cursor = cnxn.cursor()
        info = json.loads(data.decode("utf-8"))
        if len(info) > 0:
            for i in range(0, len(info["bars"])):
                diayhora = datetime.datetime.utcfromtimestamp(
                    int(info["bars"][i]['time']))
                    
                dia = str(diayhora.date().isoformat())
                
                tiempo = str(diayhora)
                
                sql_cmd = "Select * from simbolos where simbolo = '" + \
                    symbol + "' and Fecha = '" + dia + "'"
                cursor.execute(sql_cmd)
                res = cursor.fetchone()

                if res == None:
                    apertura = round(
                        0.05 * (round(info["bars"][i]['open']/0.05)), 2)
                    high = round(
                        0.05 * (round(info["bars"][i]['high']/0.05)), 2)
                    low = round(0.05 * (round(info["bars"][i]['low']/0.05)), 2)
                    cierre = round(
                        0.05 * (round(info["bars"][i]['close']/0.05)), 2)
                    if (cierre > 0.0):
                        #print(dia + " --- " + str(cierre))
                        sql_cmd = "insert into Simbolos values ('" + symbol + "', '" + dia + "', " + str(
                            apertura) + ", " + str(high) + ", " + str(low) + ", " + str(cierre) + ")"
                        cursor.execute(sql_cmd)
                        cnxn.commit()
                        comandsFile = 'SQLCMDS.txt'
                        f = open(comandsFile, 'a')
                        f.write(sql_cmd + "\n")
                        f.close()
            twoWeeksAgo = datetime.datetime.now() - datetime.timedelta(days=15)
            letters = string.ascii_lowercase
            randomname = ''.join(random.choice(letters) for i in range(15))
            filename = symbol + "-" + randomname + '.png'
            sql_cmd = "Select Fecha,Apertura,High,Low,Cierre from Simbolos where simbolo = '" + \
                symbol + "' and Fecha > '" + twoWeeksAgo.strftime("%Y-%m-%d") + "'"
            df = pd.read_sql_query(sql_cmd, cnxn)
            df.rename(columns={'Fecha': 'Date', 'Apertura': 'Open',
                            'Cierre': 'Close'}, inplace=True)
            df['Date'] = pd.to_datetime(df['Date'])
            df.set_index("Date", inplace=True)
            mpf.plot(df, type='candle', savefig=dict(
                fname=filename, dpi=100, pad_inches=0.25))
            cnxn.close()
            return filename
        else:
            return 'Error en update'
    except:
        traceback.print_exc()
        return 'Error en update'

