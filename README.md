# Uso basico

## Prerrequisitos

Todas las dependencias pueden instalarse a partir del archivo requirements.txt

```
pip install -r requirements.txt
```

## Utilizacion

Para el uso de la solucion es necesario iniciar al menos una instancia de cada objeto en el sigiente orden

1. Balanceadpr/balancer.py
2. ServerNode/server.py
3. client/client.py

### Comentarios

La clave precompartida para permitir el acceso a la base de datos por razones de simplesa se encuentra "hardcodeada" en el script "ServerNode/server.py" como la variable presharedkey

Mayores detalles del funcionamiento de la solucion en el documento "Proyecto Final del Curso.docx"
