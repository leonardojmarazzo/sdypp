import http.client
import json
import hmac
import hashlib
import base64
from datetime import datetime, timedelta

def ceil_dt(dt, delta):
    return dt + (datetime.min - dt) % delta

def apicall():
    retriesCunt = 0
    response = False
    while retriesCunt < 5 and not response:
        try: 
            conn = http.client.HTTPSConnection("api.timezonedb.com")
            conn.request("GET", "/v2.1/get-time-zone?key=DFYMFZV1MPWB&format=json&by=zone&zone=Europe/London")

            res = conn.getresponse()
            response = True
            return res.read()
        except:
            retriesCunt += 1
            time.sleep(45)
            continue

def encrypt_string(hash_string):
    sha_signature = \
        hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature

def encrypt_key(hash_string, key):
    dig = hmac.new(key.encode(), msg=hash_string.encode(), digestmod=hashlib.sha256).hexdigest()
    return dig

def getTimeMinutes():
    data = json.loads(apicall().decode())
    gmttime = datetime.strptime(data['formatted'], "%Y-%m-%d %H:%M:%S")
    return ceil_dt(gmttime, timedelta(minutes=5))
    
def getAuthenticationToken(key):
    time = getTimeMinutes().strftime('%H:%M')
    return encrypt_key(time, key)