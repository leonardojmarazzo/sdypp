import socket
import TokenGenerator
import time


def getServer(ip, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    st = 'get server'
    s.send(st.encode())
    data = s.recv(1024)
    s.close()
    return data.decode()

def calcularHash(presharedKey):
    st = TokenGenerator.getAuthenticationToken(presharedKey)
    return st


def connection(ip, port, token):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    s.send(token.encode())
    data = s.recv(1024).decode()
    if 'invalido' in data:
        s.close()
        return
    else:
        op = '1'
        while op != '0':
            print("Seleccione una opcion")
            print("1- Actualizar Datos")
            print("2- Simulacion RSI")
            print("3- Simulacion MCO")
            print("4- Editar RSI")
            print("5- Editar MCO")
            print("6- Calculo RSI")
            print("7- Calculo MCO")
            print("0- exit")
            op = input()
            ti = time.time()
            if op == '1':
                actualizacionDatos(s)
                pass
            elif op == '2':
                SimulacionRSI(s)
                pass
            elif op == '3':
                SimulacionMCO(s)
                pass
            elif op == '4':
                editarRSI(s)
                pass
            elif op == '5':
                editarMCO(s)
                pass
            elif op == '6':
                calculoRSI(s)
                pass
            elif op == '7':
                calculoMCO(s)
                pass
            elif op == '0':
                s.close()
                return
            tf = time.time() - ti
            print("Tiempo de duracion: {}".format(tf))

def actualizacionDatos(s):
    presharedkey = input("Ingrese la clave previamente compartida: ")
    simbolo = input("Ingrese el activo a actualizar: ")
    cmd = "Actualizar datos " + simbolo
    s.send(cmd.encode())
    res = s.recv(1024).decode()
    if 'token' in res.lower():
        token = calcularHash(presharedkey)
        s.send(token.encode())
        res = s.recv(1024).decode()
        
        if 'file' in res:
            s.send("ok".encode())
            filesize = int(res.split(":")[1])
            filename = simbolo + '.png'
            f = open(filename, 'wb')
            rcv = 0
            while rcv < filesize:
                l = s.recv(1024)
                rcv += 1024
                f.write(l)
                if not l:
                    break
                s.send("ok".encode())
            f.close()
            print('Archivo guardado en {}'.format(filename))
        else:
            print(res)

def SimulacionRSI(s):
    simbolo = input("Ingrese el activo a simular: ")
    rango = input("Ingrese el rango a utilizar (valores separados por coma): ")
    cmd = "Simulacion RSI {} {}".format(simbolo, rango)
    s.send(cmd.encode())
    res = s.recv(1024).decode()
    
    if 'file' in res:
        s.send("ok".encode())
        filesize = int(res.split(":")[1])
        filename = simbolo + '.csv'
        f = open(filename, 'wb')
        rcv = 0
        while rcv < filesize:
            l = s.recv(1024)
            rcv += 1024
            f.write(l)
            if not l:
                break
            s.send("ok".encode())
        f.close()
        print('Archivo guardado en {}'.format(filename))
    else:
        print(res)

def SimulacionMCO(s):
    simbolo = input("Ingrese el activo a simular: ")
    rango = input("Ingrese el rango a utilizar para la MMC (valores separados por coma): ")
    rango2 = input("Ingrese el rango a utilizar para la MML (valores separados por coma): ")
    cmd = "Simulacion MCO {} {} {}".format(simbolo, rango, rango2)
    s.send(cmd.encode())
    res = s.recv(1024).decode()
    
    if 'file' in res:
        s.send("ok".encode())
        filesize = int(res.split(":")[1])
        filename = simbolo + '.csv'
        f = open(filename, 'wb')
        rcv = 0
        while rcv < filesize:
            l = s.recv(1024)
            rcv += 1024
            f.write(l)
            if not l:
                break
            s.send("ok".encode())
        f.close()
        print('Archivo guardado en {}'.format(filename))
    else:
        print(res)

def calculoRSI(s):
    simbolo = input("Ingrese el activo a calcular: ")
    rango = input("Ingrese el periodo o 0 para usar el default: ")
    per = ""
    if rango != '0':
        per = rango
    cmd = "CALCULO RSI {} {}".format(simbolo, per)
    s.send(cmd.encode())
    res = s.recv(1024).decode()
    
    if 'file' in res:
        s.send("ok".encode())
        filesize = int(res.split(":")[1])
        filename = simbolo + '.png'
        f = open(filename, 'wb')
        rcv = 0
        while rcv < filesize:
            l = s.recv(1024)
            rcv += 1024
            f.write(l)
            if not l:
                break
            s.send("ok".encode())
        f.close()
        print('Archivo guardado en {}'.format(filename))
    else:
        print(res)

def calculoMCO(s):
    MMC = ""
    MML = ""
    simbolo = input("Ingrese el activo a calcular: ")
    rango = input("Ingrese la MMC o 0 para usar default: ")
    if rango != '0':
        MMC = "TPC:" + rango
    rango2 = input("Ingrese la MML o 0 para usar default: ")
    if rango2 != '0':
        MML = "TPL:" + rango2
    cmd = "CALCULO MCO {} {} {}".format(simbolo, MMC, MML)
    s.send(cmd.encode())
    res = s.recv(1024).decode()
    
    if 'file' in res:
        s.send("ok".encode())
        filesize = int(res.split(":")[1])
        filename = simbolo + '.png'
        f = open(filename, 'wb')
        rcv = 0
        while rcv < filesize:
            l = s.recv(1024)
            rcv += 1024
            f.write(l)
            if not l:
                break
            s.send("ok".encode())
        f.close()
        print('Archivo guardado en {}'.format(filename))
    else:
        print(res)

def editarRSI(s):
    presharedkey = input("Ingrese la clave previamente compartida: ")
    simbolo = input("Ingrese el activo a editar: ")
    periodo = input("Ingrese el nuevo periodo: ")
    cmd = "Editar RSI {} {}".format(simbolo, periodo)
    s.send(cmd.encode())
    res = s.recv(1024).decode()
    if 'token' in res.lower():
        token = calcularHash(presharedkey)
        s.send(token.encode())
        res = s.recv(1024).decode()
        print(res)

def editarMCO(s):
    presharedkey = input("Ingrese la clave previamente compartida: ")
    simbolo = input("Ingrese el activo a editar: ")
    MMC = input("Ingrese la nueva MMC: ")
    MML = input("Ingrese la nueva MML: ")

    cmd = "Editar MCO {} {} {}".format(simbolo, MMC, MML)
    s.send(cmd.encode())
    res = s.recv(1024).decode()
    if 'token' in res.lower():
        token = calcularHash(presharedkey)
        s.send(token.encode())
        res = s.recv(1024).decode()
        print(res)


def main():
    ipbal = input('Ingrese la ip del balanceador: ')
    portbal = int(input('Ingrese el puerto del balanceador: '))
    st = getServer(ipbal, portbal)
    servertoken = st.split()
    token = servertoken[1]
    ipport = servertoken[0].split(":")
    ip = ipport[0]
    port = int(ipport[1])
    connection(ip, port, token)


if __name__ == "__main__":
    main()
    pass
